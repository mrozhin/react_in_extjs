var SomeReactJsx = React.createClass({
    render: function () {
        if (this.props.expanded) {
            return <div>
                <input type="number"
                       value={this.props.displayedNumValueExpanded}
                       placeholder="Unlimited"
                       onChange={this.props.onNumValueChange}/>
                <select value={this.props.measureUnit}
                        onChange={this.props.onMeasureUnitChange}>
                    <option value="B">B</option>
                    <option value="KB">KB</option>
                    <option value="MB">MB</option>
                    <option value="GB">GB</option>
                    <option value="TB">TB</option>
                    <option value="PB">PB</option>
                </select>
            </div>;
        } else {
            return <div>
                <div onClick={this.props.onCollapsedClick}>
                    {this.props.displayedNumValueCollapsed}
                    {(function () {
                        if (this.props.numValue >= 0) {
                            return <span>{this.props.measureUnit}</span>;
                        }
                    }.bind(this)())}
                </div>
            </div>;
        }
    }
});
