Ext.define("ReactInExt.component.SomeReact", {
    extend: "Ext.Component",
    alias: "widget.some-react",
    viewModel: {
        data: {
            expanded: false,
            numValue: 1000,
            displayedNumValueCollapsed: 1000,
            displayedNumValueExpanded: 1000,
            measureUnit: "GB"
        }
    },
    onRender: function () {
        "use strict";
        this.reactRender();
        this.mon(Ext.getBody(), "click", function (event, target) {
            if (!this.getEl().contains(target)) {
                this.onExternalClicked();
            }
        }, this);
        //this.callParent();
        Ext.Component.prototype.onRender.apply(this, arguments);
    },
    reactRender: function () {
        "use strict";
        var dom = this.el.dom;
        React.render(React.createElement(this.reactClass, Ext.apply(this.getViewModel().getData(), this.getHandlers())), dom);
    },
    reactClass: React.createClass({
        /**
         * React завернул render в try/catch. Теперь дебагать свой код станет хуже.
         */
        render: function () {
            "use strict";
            var USE_JSX = true;
            var viewModel = this.props;
            /**
             * В шаблоне нельзя использовать нижний ригистр onclick, только camel.
             */
            if (USE_JSX) {
                return React.createElement(SomeReactJsx, viewModel);
            }
            return SomeReactRT.apply(viewModel);
        }
    }),
    getHandlers: function () {
        "use strict";
        //TODO each
        return {
            onCollapsedClick: this.onCollapsedClick.bind(this),
            onNumValueChange: this.onNumValueChange.bind(this),
            onMeasureUnitChange: this.onMeasureUnitChange.bind(this),
            onExternalClicked: this.onExternalClicked.bind(this)
        };
    },
    onCollapsedClick: function () {
        "use strict";
        this.viewModel.set("expanded", true);
        this.reactRender();
    },
    onNumValueChange: function (event) {
        "use strict";
        var value = event.target.value;
        /**
         * Как ввел пользователь
         */
        this.viewModel.set("displayedNumValueExpanded", value);
        this.reactRender();
    },
    onMeasureUnitChange: function (event) {
        "use strict";
        var value = event.target.value;
        this.viewModel.set("measureUnit", value);
        this.reactRender();
    },
    onExternalClicked: function () {
        "use strict";
        this.viewModel.set("expanded", false);
        var value = this.viewModel.get("displayedNumValueExpanded");
        value = (value >= 0 && value !== "") ? value : -1;
        if (value > 0) {
            value = this.getBytes(value, this.viewModel.get("measureUnit"));
        }
        /**
         * Чистое значение для передачи во внешнюю модель
         */
        this.viewModel.set("numValue", value);
        var normalized = this.normalizeValue(value);
        value = normalized[0];
        this.viewModel.set("displayedNumValueCollapsed", value >= 0 ? value : "Unlimited");
        this.viewModel.set("displayedNumValueExpanded", value >= 0 ? value : "");
        this.viewModel.set("measureUnit", normalized[1]);
        this.reactRender();
    },
    normalizeValue: function (numValue) {
        "use strict";
        var measureUnit;
        if (numValue <= 0) {
            return [numValue, "GB"];
        }

        if (numValue > this.getBytes(1, "PB")) {
            measureUnit = "PB";
        } else if (numValue > this.getBytes(1, "TB")) {
            measureUnit = "TB";
        } else if (numValue > this.getBytes(1, "GB")) {
            measureUnit = "GB";
        } else if (numValue > this.getBytes(1, "MB")) {
            measureUnit = "MB";
        } else if (numValue > this.getBytes(1, "KB")) {
            measureUnit = "KB";
        } else {
            measureUnit = "B";
        }
        return [numValue / this.getBytes(1, measureUnit), measureUnit];
    },
    getBytes: function (value, measureUnit) {
        "use strict";
        var unitSize = {
            "B": 1,
            "KB": Math.pow(1024, 1),
            "MB": Math.pow(1024, 2),
            "GB": Math.pow(1024, 3),
            "TB": Math.pow(1024, 4),
            "PB": Math.pow(1024, 5)
        };
        return value * unitSize[measureUnit];
    }
});
