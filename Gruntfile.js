module.exports = function (grunt) {
    "use strict";
    grunt.loadNpmTasks('grunt-react-templates');
    grunt.loadNpmTasks('grunt-react');
    grunt.initConfig({
        reactTemplates: {
            dist: {
                src: ['app/**/*.rt'], //glob patterns of files to be processed
                options: {
                    modules: 'none',  //possible values: (amd|commonjs|es6|typescript|none)
                    format: 'stylish' //possible values: (stylish|json)
                }
            }
        },
        react: {
            jsx: {
                files: [
                    {
                        expand: true,
                        cwd: 'app',
                        src: ['**/*.jsx'],
                        dest: 'app',
                        ext: '.jsx.js'
                    }
                ]
            }
        }
    });
    grunt.registerTask('default', ['reactTemplates', 'react']);
};
